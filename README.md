# Today Keyword
---

## 2018.04.12
* 表达式引擎
> 了解表达式引擎如何工作及相关的资料

| 关键词 | 链接 | 是否已读 | 
| :---: | :---: | :---: | 
| 关于数据级别权限关系需求分析 | [Go](https://blog.csdn.net/wabiaozia/article/details/73412204)| 未读 |
| 数据级别权限管理初步方案 | [Go](https://my.oschina.net/tinyframework/blog/653256) | 未读 |
| 关于JDBC 级别权限管理方案设计 | [Go](https://my.oschina.net/tinyframework/blog/675439) | 未读 |


* mybatis 拦截器
> 了解mybatis 拦截器如何工作

| 关键词 | 链接 | 是否已读 | 
| :---: | :---: | :---: | 
| mybatis拦截器的使用详解 | [Go](https://www.jianshu.com/p/71fcc35505e4)| 未读 |
